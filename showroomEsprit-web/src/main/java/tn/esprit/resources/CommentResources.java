package tn.esprit.resources;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import esprit.tn.entities.Comment;
import esprit.tn.services.CommentServicesLocal;

@Stateless
@Path("/comment")

public class CommentResources {

	
	@EJB
	CommentServicesLocal myService ;

	@POST
	@Path("addcomment/{idUser}/{idPost}")
	@Produces("application/json")
	@Consumes("application/json")
	public String addComment(Comment cmt,@PathParam("idUser") String idUser,@PathParam("idPost") String idPost) {
		if (myService.addComment(cmt, idUser,idPost)) {
			return " has been added";
		}else{
			return "error:verify role";
			}
	}
	@GET
	@Path("getcomments")
	@Produces("application/json")
	public List<Comment> findComment(){
		return myService.getComments();
	}

	@PUT
	@Path("updatecomment/{id}")
	@Produces("application/json")
	@Consumes("application/json")
	public String updateComment(Comment c,@PathParam("id") String id) {
		if (myService.updateComment(id,c)) {
			return " has been updated";
		}else{
			return "error";
		}
		}
	@PUT
	@Path("affectcomment2post/{idCmt}/{idPost}")
	@Produces("application/json")
	@Consumes("application/json")
	public String affectcomment2post(@PathParam("idCmt") String idCmt,@PathParam("idPost") String idPost) {
		if (myService.affectCommentToPost(idCmt, idPost)) {
			return " Comment has been affected";
		}else{
			return "error";
		}
		}

	@GET
	@Path("getcomment/{id}")
	@Produces("application/json")
	public Comment findOne(@PathParam("id") String id) {
		return myService.findCommentById(id);
	}
	@GET
	@Path("getcommentByPost/{idPost}")
	@Produces("application/json")
	public List<Comment> getcommentByPost(@PathParam("idPost") String idPost) {
		return myService.findCommentByPost(idPost);
	}
	@DELETE
	@Path("deletecomment/{id}")
	@Produces("application/json")
	public String deleteComment(@PathParam("id") String id) {
		if (myService.deleteComment(id)) {
			return " Comment has been deleted";
		}else{
			return "error";
		}
	}
	@GET
	@Path("calculatecommentsbypost/{idPost}")
	@Produces("application/json")
	public long calculatecommentsbypost(@PathParam("idPost") String idPost) {
		return myService.calculateCommentsByPost(idPost);
	}

}
