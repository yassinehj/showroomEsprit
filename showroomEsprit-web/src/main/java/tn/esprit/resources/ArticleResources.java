package tn.esprit.resources;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import esprit.tn.entities.Article;
import esprit.tn.services.ArticleServicesLocal;

@Stateless
@Path("/article")
public class ArticleResources {
@EJB
ArticleServicesLocal myService ;

@POST
@Path("addArticle/{idUser}/{idShowroom}")
@Produces("application/json")
@Consumes("application/json")
public String addArticle(Article art,@PathParam("idUser") String idUser,@PathParam("idShowroom") String idShowroom) {
	if (myService.addArticle(art, idUser,idShowroom)) {
		return " has been added";
	}else{
		return "error:verify role";
		}
}
@GET
@Path("getArticles")
@Produces("application/json")
public List<Article> findArticles(){
	return myService.getArticles();
}
@GET
@Path("getArticlesByUser/{idUser}")
@Produces("application/json")
public List<Article> findArticlesByUser(@PathParam("idUser") String idUser){
	return myService.getArticlesByUser(idUser);
}
@GET
@Path("getArticlesByShowroom/{idShowroom}")
@Produces("application/json")
public List<Article> findArticlesByShowroom(@PathParam("idShowroom") String idShowroom){
	return myService.getArticlesByShowroom(idShowroom);
}
@GET
@Path("getArticlesByShowroomByArtist/{idShowroom}/{idArtist}")
@Produces("application/json")
public List<Article> findArticlesByShowroomByArtist(@PathParam("idShowroom") String idShowroom,@PathParam("idArtist") String idArtist){
	return myService.getArticlesByShowroomByArtist(idShowroom, idArtist);
}

@PUT
@Path("updateArticle/{id}")
@Produces("application/json")
@Consumes("application/json")
public String updateArticle(Article art,@PathParam("id") String id) {
	if (myService.updateArticle(id,art)) {
		return " has been updated";
	}else{
		return "error";
	}
	}
@PUT
@Path("articleCommander/{id}")
@Produces("application/json")
public String articleCommander(@PathParam("id") String id) {
	if (myService.articleCommander(id)) {
		return " has been updated";
	}else{
		return "error";
	}
	}
@PUT
@Path("articleNonCommander/{id}")
@Produces("application/json")
public String articleNonCommander(@PathParam("id") String id) {
	if (myService.articleNonCommander(id)) {
		return " has been updated";
	}else{
		return "error";
	}
	}
/*@PUT
@Path("affecterArticle2commande/{idArt}/{idComm}")
@Produces("application/json")
@Consumes("application/json")
public String affecterArticle2Commande(@PathParam("idArt") String idArt,@PathParam("idComm") String idComm) {
	if (myService.affecterArticleToCommande(idArt, idComm)) {
		return " article has affected";
	}else{
		return "error";
	}
	}
*/
@GET
@Path("getArticle/{id}")
@Produces("application/json")
public Article findOne(@PathParam("id") String id) {
	return myService.findArticleById(id);
}
@DELETE
@Path("deleteArticle/{id}")
@Produces("application/json")
public String deleteArticle(@PathParam("id") String id) {
	if (myService.deleteArticle(id)) {
		return " user has been deleted";
	}else{
		return "error";
	}
}
}

