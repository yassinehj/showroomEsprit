package tn.esprit.resources;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import esprit.tn.entities.Article;
import esprit.tn.entities.Showroom;
import esprit.tn.entities.Users;
import esprit.tn.services.ShowroomServicesLocal;


@Stateless
@Path("/showroom")
public class ShowroomResources {

	@EJB
	ShowroomServicesLocal myService ;

	@POST
	@Path("addShowroom/{idUser}")
	@Produces("application/json")
	@Consumes("application/json")
	public String addShowroom(Showroom sh,@PathParam("idUser") String idUser) {
		if (myService.addShowroom(sh, idUser)) {
		return "has been added";
	}else{
		return "error";
		}
	}
	
	@GET
	@Path("getShowrooms")
	@Produces("application/json")
	public List<Showroom> findShowrooms(){
		return myService.getShowroom();
	}

	@PUT
	@Path("updateShowroom/{id}")
	@Produces("application/json")
	@Consumes("application/json")
	public String updateShowroom(Showroom sh ,@PathParam("id") String id) {
		if (myService.updateShowroom(id,sh)) {
			return " has been updated";
		}else{
			return "error";
		}
		}

	@GET
	@Path("getShowroom/{id}")
	@Produces("application/json")
	public Showroom findOne(@PathParam("id") String id) {
		return myService.findShowroomById(id);
	}
	@DELETE
	@Path("deleteShowroom/{id}")
	@Produces("application/json")
	public String deleteShowroom(@PathParam("id") String id) {
		if (myService.deleteShowroom(id)) {
			return " showroom has been deleted";
		}else{
			return "error";
		}
	}
	@GET
	@Path("getShowroomByUser/{idUser}")
	@Produces("application/json")
	public List<Showroom> findShowroomsByUser(@PathParam("idUser") String idUser){
		return myService.getShowroomByUser(idUser);
	}
	
}
