package tn.esprit.resources;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import esprit.tn.entities.Post;
import esprit.tn.entities.ReservationShowroom;
import esprit.tn.services.PostServicesLocal;

@Stateless
@Path("/post")
public class PostResources {

	@EJB
	PostServicesLocal myService ;
	
	
	@POST
	@Path("addpost/{idUser}")
 @Produces("application/json")
	@Consumes("application/json")
	public String addpost(Post pst,@PathParam("idUser") String idUser) {
		if (myService.addPost(pst, idUser)) {
		return "has been added";
	}else{
		return "error";
		}
	}
	
	@GET
	@Path("getposts")
	@Produces("application/json")
	public List<Post> findpost(){
		return myService.getPost();
	}
	
	@PUT
	@Path("updatepost/{id}")
	@Produces("application/json")
	@Consumes("application/json")
	public String editpost(Post p,@PathParam("id") String id) {
		if (myService.updatePost(id,p)) {
			return " has been updated";
		}else{
			return "error";
		}
		}
	
	@GET
	@Path("getpost/{id}")
	@Produces("application/json")
	public Post findOne(@PathParam("id") String id) {
		return myService.findPostById(id);
	}
	
	@DELETE
	@Path("deletepost/{id}")
	@Produces("application/json")
	public String removepost(@PathParam("id") String id) {
		if (myService.deletePost(id)) {
			return " post has been deleted";
		}else{
			return "error";
		}
	}
	@GET
	@Path("getposttouser/{idUser}")
	@Produces("application/json")
	public List<Post> findPostByUser(@PathParam("idUser") String idUser){
		return myService.listPostToUser(idUser);
	}
	
	
	
}
