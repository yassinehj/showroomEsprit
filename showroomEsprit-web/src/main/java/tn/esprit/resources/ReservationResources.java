package tn.esprit.resources;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import esprit.tn.entities.Article;
import esprit.tn.entities.ReservationShowroom;
import esprit.tn.entities.Showroom;
import esprit.tn.entities.Users;
import esprit.tn.services.ReservationServicesLocal;
import esprit.tn.services.ShowroomServicesLocal;


@Stateless
@Path("/reservation")
public class ReservationResources {

	@EJB
	ReservationServicesLocal myService ;

	@POST
	@Path("reserver/{idShowroom}/{idUser}")
	@Produces("application/json")
	@Consumes("application/json")
	public String addShowroom(ReservationShowroom r,@PathParam("idShowroom") String idShowroom,@PathParam("idUser") String idUser) {
		if (myService.reserver(r, idUser, idShowroom)) {
		return "has been added";
	}else{
		return "error";
		}
	}
	
	@GET
	@Path("getShowroomsreserverByUser/{idArtist}")
	@Produces("application/json")
	public List<ReservationShowroom> findShowroomsreserverByUser(@PathParam("idArtist") String idArtist){
		return myService.listShowroomReseverToArtist(idArtist);
	}
	@GET
	@Path("listReservationShowroomofGallery/{idGallery}")
	@Produces("application/json")
	public List<ReservationShowroom> listReservationShowroomofGallery(@PathParam("idGallery") String idGallery){
		return myService.listReservationShowroomofGallery(idGallery);
	}
	@GET
	@Path("getShowroomsreserver/{idGalleryowner}")
	@Produces("application/json")
	public List<ReservationShowroom> findShowroomsreserver(@PathParam("idGalleryowner") String idGalleryowner){
		return myService.listShowroomResever(idGalleryowner);
	}
	@GET
	@Path("getShowroomsreserverToCustomer")
	@Produces("application/json")
	public List<ReservationShowroom> findShowroomsreserverToCustomer(){
		return myService.listShowroomReseverToCustomer();
	}
	@DELETE
	@Path("deleteReservation/{id}")
	@Produces("application/json")
	public String deleteReservation(@PathParam("id") String id) {
		if (myService.deleteReservation(id)) {
			return " user has been deleted";
		}else{
			return "error";
		}
	}
	@PUT
	@Path("updateReservation/{id}")
	@Produces("application/json")
	@Consumes("application/json")
	public String updateReservation(ReservationShowroom art,@PathParam("id") String id) {
		if (myService.updateReservation(id, art)) {
			return " has been updated";
		}else{
			return "error";
		}
		}
	@GET
	@Path("getReservation/{id}")
	@Produces("application/json")
	public ReservationShowroom findOne(@PathParam("id") String id) {
		return myService.findReservationById(id);
	}
	@GET
	@Path("verifyReservation/{dateInString}")
	@Produces("text/html")
	public String verifyReservation(@PathParam("dateInString")String dateInString) {
		if(myService.verifyShowroomReserver(dateInString).equals("ok"))
			return "ok";
		else if(myService.verifyShowroomReserver(dateInString).equals("reserver"))
			return "reserver";
		else
			return "erreur format";
	}
	@GET
	@Path("calculatereservationbyshowroom/{idShowroom}")
	@Produces("application/json")
	public long calculatecommentsbypost(@PathParam("idShowroom") String idShowroom) {
		return myService.calculateReservationByShowroom(idShowroom);
	}
	@GET
	@Path("calculatereservationbyshowroombygallery/{idShowroom}/{idUser}")
	@Produces("application/json")
	public long calculatereservationbyshowroombyuser(@PathParam("idShowroom") String idShowroom,@PathParam("idUser") String idUser) {
		return myService.calculateReservationByShowroomByGallery(idShowroom, idUser);
	}

	/*
	@PUT
	@Path("updateShowroom/{id}")
	@Produces("application/json")
	@Consumes("application/json")
	public String updateShowroom(@PathParam("id") String id) {
		if (myService.updateShowroom(id)) {
			return " has been updated";
		}else{
			return "error";
		}
		}

	@GET
	@Path("getShowroom/{id}")
	@Produces("application/json")
	public Showroom findOne(@PathParam("id") String id) {
		return myService.findShowroomById(id);
	}
	@DELETE
	@Path("deleteShowroom/{id}")
	@Produces("application/json")
	public String deleteShowroom(@PathParam("id") String id) {
		if (myService.deleteShowroom(id)) {
			return " showroom has been deleted";
		}else{
			return "error";
		}
	}
	@GET
	@Path("getShowroomByUser/{idUser}")
	@Produces("application/json")
	public List<Showroom> findShowroomsByUser(@PathParam("idUser") String idUser){
		return myService.getShowroomByUser(idUser);
	}*/
	
}
