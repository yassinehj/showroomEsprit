package tn.esprit.resources;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import esprit.tn.entities.Article;
import esprit.tn.entities.Event;
import esprit.tn.entities.Users;
import esprit.tn.services.ArticleServicesLocal;
import esprit.tn.services.EventServicesLocal;

@Stateless
@Path("/Event")
public class EventResources {
@EJB
EventServicesLocal myService ;

@POST
@Path("addEvent/{idShowroom}")
@Consumes("application/json")
public String addEvent(Event e,@PathParam("idShowroom") String idShowroom) {
	if (myService.addEvent(e,idShowroom)) {
		return " has been added";
	}else{
		return "error:verify role";
		}
}
//@GET
//@Path("getArticles")
//@Produces("application/xml")
//public List<Article> findArticles(){
//	return myService.getArticles();
//}
//
@PUT
@Path("AffecterUserEvent/{id}/{idUser}")
@Produces("application/json")
@Consumes("application/json")
public String affecterUserToEvent(@PathParam("id") String id,@PathParam("idUser") String idUser) {
	if( myService.affecterUserToEvent(id, idUser)){
		return "user has been affected";
	}else{
		return "error";
	}
	}
//
@GET
@Path("getEvent/{id}")
@Produces("application/json")
public Event findOne(@PathParam("id") String id) {
	return myService.findEventById(id);
}
@GET
@Path("getAllEvents")
@Produces("application/json")
public List<Event> findAllEvents() {
	return myService.getAllEvents();
}
@GET
@Path("getEventsByShowroom/{idShowroom}")
@Produces("application/json")
public List<Event> findEventsByShowroom(@PathParam("idShowroom") String idShowroom) {
	return myService.getEventsByShowroom(idShowroom);
}
@GET
@Path("getEventByUser/{idUser}")
@Produces("application/json")
public List<Event> findEventByUser(@PathParam("idUser") String idUser) {
	return myService.findEventByUser(idUser);
}
@PUT
@Path("updateEvent/{id}")
@Produces("application/json")
@Consumes("application/json")
public String updateEvent(Event e,@PathParam("id") String id) {
	if (myService.updateEvent(id, e)) {
		return " has been updated";
	}else{
		return "error";
	}
	}
@DELETE
@Path("deleteEvent/{id}")
@Produces("application/json")
public String deleteEvent(@PathParam("id") String id) {
	if (myService.deleteEvent(id)) {
		return " user has been deleted";
	}else{
		return "error";
	}
}
@GET
@Path("calculateeventbyshowroom/{idShowroom}")
@Produces("application/json")
public long calculateEventByShowroom(@PathParam("idShowroom") String idShowroom) {
	return myService.calculateEventByShowroom(idShowroom);
}
//@DELETE
//@Path("deleteArticle/{id}")
//@Produces("application/xml")
//public String deleteArticle(@PathParam("id") String id) {
//	if (myService.deleteArticle(id)) {
//		return " user has been deleted";
//	}else{
//		return "error";
//	}
//}
}

