package tn.esprit.resources;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import esprit.tn.entities.Users;
import esprit.tn.services.UserServicesLocal;

@Stateless
@Path("/user")
public class UserResources {
@EJB
UserServicesLocal myService ;

@POST
@Path("adduser")
@Consumes("application/json")
public String addUser(Users u) {
	if (myService.addUser(u)) {
		return " has been added";
	}else{
		return "error:verify role";
		}
}
@GET
@Path("getUsers")
@Produces("application/json")
public List<Users> findUsers(){
	return myService.getUsers();
}

@PUT
@Path("updateUser/{id}")
@Produces("application/json")
@Consumes("application/json")
public String updateUser(Users u,@PathParam("id") String id) {
	if (myService.updateUsers(id,u)) {
		return " has been updated";
	}else{
		return "error:verify role";
	}
	}

@GET
@Path("getUser/{id}")
@Produces("application/json")
public Users findOne(@PathParam("id") String id) {
	return myService.findById(id);
}
@DELETE
@Path("deleteUser/{id}")
@Produces("application/json")
public String deleteUser(@PathParam("id") String id) {
	if (myService.deleteUsers(id)) {
		return " user has been deleted";
	}else{
		return "error";
	}
}
@GET
@Path("authen/{login}/{password}")
@Produces("text/html")
public String auth(@PathParam("login") String login,@PathParam("password") String password)  {
	if ((myService.authentificationUser(login, password)!=null) && myService.authentificationUser(login, password).getRole().isCustomer()) {
		
		return "customer";
	}
	else if ((myService.authentificationUser(login, password)!=null) && myService.authentificationUser(login, password).getRole().isGalleryOwner()) {
		
		return "galeryOwner";
	}
	else if ((myService.authentificationUser(login, password)!=null) && myService.authentificationUser(login, password).getRole().isArtist()) {
		//JSONObject jsonObj = new JSONObject("{\"role\":\"N45595\"}");
		return "artist";
	}
	else if ((myService.authentificationUser(login, password)!=null) && myService.authentificationUser(login, password).getRole().isAdministrateur()) {
		//JSONObject jsonObj = new JSONObject("{\"role\":\"55478\"}");
		return "administrateur";
	}
	
	else{
		//JSONObject jsonObj = new JSONObject("{\"role\":\"5445\"}");
		return "failed";
	}
}
}

