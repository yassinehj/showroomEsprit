package tn.esprit.resources;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import esprit.tn.entities.Commande;
import esprit.tn.entities.Post;
import esprit.tn.entities.Users;
import esprit.tn.services.CommandeServicesLocal;

@Stateless
@Path("/commande")
public class CommandeResources {
	@EJB
	CommandeServicesLocal myService ;

	@POST
	@Path("addCommande/{idUser}/{idArticle}")
	@Produces("application/json")
	@Consumes("application/json")
	public String addCommande(@PathParam("idUser") String idUser,@PathParam("idArticle") String idArticle) {
		if (myService.addCommande(idUser,idArticle)) {
			return " has been added";
		}else{
			return "error:verify role";
			}
	}
	@GET
	@Path("getCommandes")
	@Produces("application/json")
	public List<Commande> findCommande(){
		return myService.getCommande();
	}
	
	
	@GET
	@Path("getCommande/{id}")
	@Produces("application/json")
	public Commande findOne(@PathParam("id") String id) {
		return myService.findCommandeById(id);
	}
	@GET
	@Path("getCommandeByArtist/{id}")
	@Produces("application/json")
	public List<Commande> getCommandeByArtist(@PathParam("id") String id) {
		return myService.getCommandeByArtist(id);
	}
	@PUT
	@Path("updateCommande/{id}")
	@Produces("application/json")
	@Consumes("application/json")
	public String updateCommande(@PathParam("id") String id) {
		if (myService.updateCommande(id)) {
			return " has been updated";
		}else{
			return "error";
		}
		}
	@DELETE
	@Path("deleteCommande/{id}")
	@Produces("application/json")
	public String deleteCommande(@PathParam("id") String id) {
		if (myService.deleteCommande(id)) {
			return " commande has been deleted";
		}else{
			return "error";
		}
	}
	@GET
	@Path("getcommandestocustomer/{idUser}")
	@Produces("application/json")
	public List<Commande> findCommandesByUser(@PathParam("idUser") String idUser){
		return myService.listCommandesToCustomer(idUser);
	}
}
