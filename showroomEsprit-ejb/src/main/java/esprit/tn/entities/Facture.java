package esprit.tn.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Facture
 *
 */
@Entity

public class Facture implements Serializable {

	   
	@Id
	private int id;
	@OneToOne
	@JoinColumn(name = "idCommande")
	private Commande idCommande;
	private int tva;
	private double prixFacture;
	private static final long serialVersionUID = 1L;

	public Facture() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	
	public int getTva() {
		return this.tva;
	}

	public void setTva(int tva) {
		this.tva = tva;
	}
	
	public double getPrixFacture() {
		return prixFacture;
	}
	public void setPrixFacture(double prixFacture) {
		this.prixFacture = prixFacture;
	}
	public Commande getIdCommande() {
		return idCommande;
	}
	public void setIdCommande(Commande idCommande) {
		this.idCommande = idCommande;
	}
	
   
}
