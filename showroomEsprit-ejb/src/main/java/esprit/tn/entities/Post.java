package esprit.tn.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@XmlRootElement
public class Post implements Serializable{

	@Id
	@GeneratedValue
	private Integer idPost;
	private String title;
	private String description;
	//private Integer idComment;
	@ManyToOne
	@JoinColumn(name="users")
	private Users users;
	
	@OneToMany(mappedBy="post",fetch=FetchType.LAZY)
	private List<Comment> comments=new ArrayList<>() ;
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public Post() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getIdPost() {
		return idPost;
	}

	public void setIdPost(Integer idPost) {
		this.idPost = idPost;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}
	@XmlTransient
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}


}
