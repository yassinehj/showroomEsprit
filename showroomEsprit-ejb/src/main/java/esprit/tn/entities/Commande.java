package esprit.tn.entities;

import java.io.Serializable;
//import java.lang.Double;
import java.lang.Integer;
//import java.util.Collection;
import java.util.Date;
//import java.util.List;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Entity implementation class for Entity: Commande
 *
 */
@Entity
@XmlRootElement
public class Commande implements Serializable {

	   
	@Id
	@GeneratedValue
	private Integer idCommande;
	
	@OneToOne
	@JoinColumn(name="fk_Article")
	private Article fk_Article;
	
	@OneToOne(mappedBy="idCommande")
	private Facture fk_Facture;
	@Temporal(TemporalType.DATE)
	private Date dateCommande;
	@ManyToOne
	@JoinColumn(name="customers")
	private Users customers ;
	
	private static final long serialVersionUID = 1L;

	public Commande() {
		super();
	}   
	
  
	
	/*public Integer getFk_Article() {
		return this.fk_Article;
	}

	public void setFk_Article(Integer fk_Article) {
		this.fk_Article = fk_Article;
	}   */
	
	
	
	public Date getDateCommande() {
		return this.dateCommande;
	}

	public Integer getIdCommande() {
		return idCommande;
	}



	public void setIdCommande(Integer idCommande) {
		this.idCommande = idCommande;
	}



	public Article getFk_Article() {
		return fk_Article;
	}



	public void setFk_Article(Article fk_Article) {
		this.fk_Article = fk_Article;
	}



	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}
	public Users getCustomers() {
		return customers;
	}
	public void setCustomers(Users customers) {
		this.customers = customers;
	}
	@XmlTransient
	public Facture getFk_Facture() {
		return fk_Facture;
	}
	public void setFk_Facture(Facture fk_Facture) {
		this.fk_Facture = fk_Facture;
	}
	
	
   
}
