package esprit.tn.entities;

import java.io.Serializable;
import java.lang.Integer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.ws.rs.ext.ParamConverter.Lazy;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import esprit.tn.entities.Article;

/**
 * Entity implementation class for Entity: Users
 *
 */
@Entity
@XmlRootElement
public class Users implements Serializable {

	   
	@Id
	@GeneratedValue
	private Integer id;
	private String name;
	private String lastName;
	private String adress;
	@Temporal(TemporalType.DATE)
	private Date birthDate;
	private String mail;
	private String login;
	private String password;
	private String numTel;
	private String field;
	private Boolean enable;
	@NotNull
	private Role role;
	
	@OneToMany(mappedBy="artists",fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	private List<Article> articles=new ArrayList<>() ;
	
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="Participate")
	private List<Event> events=new ArrayList<Event>();
	
	@OneToMany(mappedBy="users",fetch=FetchType.LAZY)
	private List<Showroom> showroom=new ArrayList<>() ;
	
	@OneToMany(mappedBy="users",fetch=FetchType.LAZY)
	private List<Post> posts=new ArrayList<>() ;
	
	@OneToMany(mappedBy="users",fetch=FetchType.LAZY)
	private List<Comment> comments;

	
	@OneToMany(mappedBy="customers",fetch=FetchType.LAZY)
	private List<Commande> commandes;
	
	
	@OneToMany(mappedBy="artist",fetch=FetchType.LAZY)
	private List<ReservationShowroom> reservation;
	

	private static final long serialVersionUID = 1L;

	public Users() {
		super();
	}   
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNumTel() {
		return numTel;
	}
	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	
	
	public Boolean getEnable() {
		return enable;
	}
	public void setEnable(Boolean enable) {
		this.enable = enable;
	}
	@XmlTransient
	public List<Article> getArticles() {
		return articles;
	}
	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
	@XmlTransient
	public List<Event> getEvents() {
		return events;
	}
	public void setEvents(List<Event> events) {
		this.events = events;
	}
	@XmlTransient
	public List<Showroom> getShowroom() {
		return showroom;
	}
	public void setShowroom(List<Showroom> showroom) {
		this.showroom = showroom;
	}
	@XmlTransient
	public List<Post> getPosts() {
		return posts;
	}
	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}
	@XmlTransient
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	@XmlTransient
	public List<Commande> getCommandes() {
		return commandes;
	}
	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}
	@XmlTransient
	public List<ReservationShowroom> getReservation() {
		return reservation;
	}
	public void setReservation(List<ReservationShowroom> reservation) {
		this.reservation = reservation;
	}
	
	
   
}
