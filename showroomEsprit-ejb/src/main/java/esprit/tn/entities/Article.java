package esprit.tn.entities;

import java.io.Serializable;
import java.lang.String;


import javax.persistence.*;
//import javax.ws.rs.ext.ParamConverter.Lazy;
//import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Entity implementation class for Entity: Article
 *
 */
@Entity
@XmlRootElement
public class Article implements Serializable {

	   
	@Id
	@GeneratedValue
	private Integer idArticle;
	
	private String articleName;
	private String articleDescription;
	private double prix;
	private String imageUrl ;
	private int commander;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="artists")
	private Users artists ;
	@OneToOne(mappedBy="fk_Article")
	private Commande commande ;
	@ManyToOne
	@JoinColumn(name="showroom")
	private Showroom showroom ;
	
	/*private List<Event> events ;*/
	private static final long serialVersionUID = 1L;

	public Article() {
		super();
	}   
	public Integer getIdArticle() {
		return this.idArticle;
	}

	public void setIdArticle(Integer idArticle) {
		this.idArticle = idArticle;
	}   
	public String getArticleName() {
		return this.articleName;
	}

	public void setArticleName(String articleName) {
		this.articleName = articleName;
	}   
	public String getArticleDescription() {
		return this.articleDescription;
	}

	public void setArticleDescription(String articleDescription) {
		this.articleDescription = articleDescription;
	}
	
	
	public int getCommander() {
		return commander;
	}
	public void setCommander(int commander) {
		this.commander = commander;
	}
	/*public List<Event> getEvents() {
		return events;
	}
	public void setEvents(List<Event> events) {
		this.events = events;
	}*/
	public Showroom getShowroom() {
		return showroom;
	}
	public void setShowroom(Showroom showroom) {
		this.showroom = showroom;
	}
	
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	public Users getArtists() {
		return artists;
	}
	public void setArtists(Users artists) {
		this.artists = artists;
	}
	@XmlTransient
	public Commande getCommande() {
		return commande;
	}
	public void setCommande(Commande commande) {
		this.commande = commande;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
   
}
