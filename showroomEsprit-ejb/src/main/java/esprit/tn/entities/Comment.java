package esprit.tn.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@XmlRootElement
public class Comment implements Serializable{

	@Id
	@GeneratedValue
	private Integer idComment;
	private String description;
	//private Integer idPost;
	@ManyToOne
	@JoinColumn(name="post")
	private Post post;
	@ManyToOne
	@JoinColumn(name="users")
	private Users users ;

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Comment() {
		super();
	
	}

	public Integer getIdComment() {
		return idComment;
	}

	public void setIdComment(Integer idComment) {
		this.idComment = idComment;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	

}
