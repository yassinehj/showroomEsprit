package esprit.tn.entities;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity implementation class for Entity: Event
 *
 */
@Entity
@XmlRootElement
public class Event implements Serializable {

	   
	@Id
	@GeneratedValue
	private Integer idEvent;
	private String descriptionEvent;
	@Temporal(TemporalType.DATE)
	private Date dateEvent;
	
	private Integer placeNumber;
	
	@ManyToMany(mappedBy="events",fetch=FetchType.EAGER)
	private List<Users> customers;
	@ManyToOne
	@JoinColumn(name="showroom")
	private Showroom showroom ;

	private static final long serialVersionUID = 1L;

	public Event() {
		super();
	}   
	public Integer getIdEvent() {
		return this.idEvent;
	}

	public void setIdEvent(Integer idEvent) {
		this.idEvent = idEvent;
	}   
	public String getDescriptionEvent() {
		return this.descriptionEvent;
	}

	public void setDescriptionEvent(String descriptionEvent) {
		this.descriptionEvent = descriptionEvent;
	}   
	   
	
	public Date getDateEvent() {
		return dateEvent;
	}
	public void setDateEvent(Date dateEvent) {
		this.dateEvent = dateEvent;
	}
	
	public Integer getPlaceNumber() {
		return this.placeNumber;
	}

	public void setPlaceNumber(Integer placeNumber) {
		this.placeNumber = placeNumber;
	}
	public List<Users> getCustomers() {
		return customers;
	}
	public void setCustomers(List<Users> customers) {
		this.customers = customers;
	}
	
	
	public Showroom getShowroom() {
		return showroom;
	}
	public void setShowroom(Showroom showroom) {
		this.showroom = showroom;
	}
	
	
}
