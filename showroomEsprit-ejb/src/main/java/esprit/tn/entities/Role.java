package esprit.tn.entities;

public enum Role {
	Administrateur,
	Artist,
	Customer,
	GalleryOwner;
	public boolean isCustomer() {
		return Customer.equals(this);
		
	}
	public boolean isAdministrateur() {
		return Administrateur.equals(this);
		
	}
	public boolean isArtist() {
		return Artist.equals(this);
		
	}
	public boolean isGalleryOwner() {
		return GalleryOwner.equals(this);
		
	}

}
