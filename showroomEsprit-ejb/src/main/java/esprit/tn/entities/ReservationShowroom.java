package esprit.tn.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity implementation class for Entity: ReservationShowroom
 *
 */
@Entity
@XmlRootElement
public class ReservationShowroom implements Serializable {

	   
	@Id
	@GeneratedValue
	private int id;
	
	@ManyToOne
	@JoinColumn(name="showroom")
	private Showroom showroom;
	
	@ManyToOne
	@JoinColumn(name="artist")
	private Users artist;
	@Temporal(TemporalType.DATE)
	private Date dateReservation;
	
	private static final long serialVersionUID = 1L;

	public ReservationShowroom() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	} 
	
	public Date getDateReservation() {
		return dateReservation;
	}
	public void setDateReservation(Date dateReservation) {
		this.dateReservation = dateReservation;
	}
	public Showroom getShowroom() {
		return showroom;
	}
	public void setShowroom(Showroom showroom) {
		this.showroom = showroom;
	}
	public Users getArtist() {
		return artist;
	}
	public void setArtist(Users artist) {
		this.artist = artist;
	}
	
	
   
}
