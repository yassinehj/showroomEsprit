package esprit.tn.services;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import esprit.tn.entities.Article;
import esprit.tn.entities.Commande;

import esprit.tn.entities.Users;

/**
 * Session Bean implementation class CommandeServices
 */
@Stateless

public class CommandeServices implements CommandeServicesLocal {
	@PersistenceContext(unitName="showroomEsprit-ejb")
	EntityManager em;
	@EJB
	UserServicesLocal serviceUser;
	@EJB
	ArticleServicesLocal serviceArticle;
    /**
     * Default constructor. 
     */
    public CommandeServices() {
        // TODO Auto-generated constructor stub
    }
    

	@Override
	public boolean addCommande(String idUser,String idArticle) {
		Commande cmd=new Commande();
		Users customers=serviceUser.findById(idUser);
		Article art=serviceArticle.findArticleById(idArticle);
		cmd.setCustomers(customers);
		cmd.setFk_Article(art);
		Date  now=new Date();
		cmd.setDateCommande(now);
		em.persist(cmd);
		return true;
	}

	@Override
	public Commande findCommandeById(String id) {
		Query q=em.createQuery("Select c from Commande c where c.idCommande= :id").setParameter("id", Integer.parseInt(id));
		
		//art.getArtists().;
		return (Commande) q.getSingleResult();
	}

	@Override
	public boolean updateCommande(String id) {
		Commande cmd=findCommandeById(id);
    	em.merge(cmd);
    	return true;
	}

	@Override
	public boolean deleteCommande(String id) {
		Query q=em.createQuery("delete from Commande c where c.idCommande= :id").setParameter("id", Integer.parseInt(id));
		q.executeUpdate();
		return true;
	}

	@Override
	public List<Commande> getCommande() {
		Query q=em.createQuery("select c from Commande c");
		return q.getResultList();
		
	}
	@Override
	public List<Commande> getCommandeByArtist(String idArtist) {
		Users artist=serviceUser.findById(idArtist);
		Query q=em.createQuery("select c from Commande c where c.fk_Article.artists=:artist").setParameter("artist", artist);
		return q.getResultList();
		
	}
	@Override
	 public List<Commande> listCommandesToCustomer(String idCustomer) {
			Users u=serviceUser.findById(idCustomer);

			Query q=em.createQuery("select c from Commande c  where c.customers = :customers ").setParameter("customers", u);

			return q.getResultList();

}

}

