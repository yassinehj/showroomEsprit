package esprit.tn.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import esprit.tn.entities.Article;
import esprit.tn.entities.Event;
import esprit.tn.entities.ReservationShowroom;
import esprit.tn.entities.Showroom;
import esprit.tn.entities.Users;

/**
 * Session Bean implementation class EventServices
 */
@Stateless
public class EventServices implements EventServicesLocal {

	@PersistenceContext(unitName="showroomEsprit-ejb")
	EntityManager em;
	@EJB
	UserServicesLocal user;
	@EJB
	ShowroomServicesLocal showroom;
    /**
     * Default constructor. 
     */
    public EventServices() {
        // TODO Auto-generated constructor stub
    }

	public boolean addEvent(Event e,String idShowroom) {
//		Users users=user.findById(idUser);
//		List<Users> l = new ArrayList<>();
//		l.add(users);
//		e.setCustomers(l);
		Showroom sh=showroom.findShowroomById(idShowroom);
		e.setShowroom(sh);
		em.persist(e);
		return true;
	}
	public Event findEventById(String id) {
		Query q=em.createQuery("Select e from Event e where e.idEvent= :id").setParameter("id", Integer.parseInt(id));
		return  (Event) q.getSingleResult();
	}
	public List<Event> findEventByUser(String idUser) {
		Users u=user.findById(idUser);
		Query q=em.createQuery("Select e from Event e where :user member of e.customers").setParameter("user", u);
		return q.getResultList();
	}

	@Override
	
	public boolean affecterUserToEvent(String id, String idUser) {
		Users u=user.findById(idUser);
		Event e=findEventById(id);
		if(u.getEvents()==null)
			u.setEvents(new ArrayList<Event>());
		u.getEvents().add(e);
		if(e.getCustomers() == null)
			e.setCustomers(new ArrayList<Users>());
		e.getCustomers().add(u);
//		List<Users> l=e.getCustomers();
//		if(l == null) {
//			l=new ArrayList<Users>();
//		}
//		l.add(u);
//		e.setCustomers(l);
		//em.merge(e);
		em.merge(u);
		return true;
	}
	@Override
	public boolean updateEvent(String id,Event event) {
	   	Event ev=findEventById(id);
    	ev.setDateEvent(event.getDateEvent());
    	ev.setDescriptionEvent(event.getDescriptionEvent());
    	ev.setPlaceNumber(event.getPlaceNumber());
    	em.merge(ev);
    	return true;
	}
	  @Override
	    public boolean deleteEvent(String id) {
			Query q=em.createQuery("delete from Event r where r.idEvent= :id").setParameter("id", Integer.parseInt(id));
			q.executeUpdate();
			return true;
	    }
	@Override
	public List<Event> getAllEvents( ) {
		Query q=em.createQuery("select e from Event e");
		return q.getResultList();
	}
	@Override
	public List<Event> getEventsByShowroom(String idShowroom ) {
		Showroom sh=showroom.findShowroomById(idShowroom);
		Query q=em.createQuery("select e from Event e where e.showroom=:sh").setParameter("sh", sh);
		return q.getResultList();
	}
	@Override
	public long calculateEventByShowroom(String idShowroom) {
		Showroom sh=showroom.findShowroomById(idShowroom);
		Query q=em.createQuery("SELECT COUNT(e.idEvent) FROM Event e WHERE e.showroom=:sh").setParameter("sh", sh);
		long evt= (long)q.getSingleResult();
		return evt;
	}

}
