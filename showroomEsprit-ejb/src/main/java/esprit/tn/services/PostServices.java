package esprit.tn.services;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import esprit.tn.entities.Comment;
import esprit.tn.entities.Post;
import esprit.tn.entities.ReservationShowroom;
import esprit.tn.entities.Users;

/**
 * Session Bean implementation class PostService
 */
@Stateless
@LocalBean
public class PostServices implements PostServicesLocal {

	
	@PersistenceContext(unitName="showroomEsprit-ejb")
	EntityManager em;
	@EJB
	UserServicesLocal usl;
	@EJB
	CommentServicesLocal cmt;
    /**
     * Default constructor. 
     */
    public PostServices() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public boolean addPost(Post ct, String idUser) {
		Users users = usl.findById(idUser);
		ct.setUsers(users);
		em.persist(ct);
		return true;
	}
	@Override
	 public List<Post> listPostToUser(String idUser) {
			Users u=usl.findById(idUser);
			Query q=em.createQuery("select r from Post r  where r.users = :users ").setParameter("users", u);
			return q.getResultList();
			
		}
	@Override
	public List<Post> getPost() {
		Query q=em.createQuery("select ct from Post ct");
		return q.getResultList();
	}

	@Override
	public Post findPostById(String id) {
		Query q=em.createQuery("Select ct from Post ct where ct.idPost= :id").setParameter("id", Integer.parseInt(id));
		return (Post) q.getSingleResult();
	}

	@Override
	public boolean updatePost(String id,Post p) {

	Post post=findPostById(id);

    	post.setTitle(p.getTitle());
    	post.setDescription(p.getDescription());
    	
    	em.merge(post);
    	return true;
	}

	@Override
	public boolean deletePost(String id) {
		List<Comment> comments=cmt.findCommentByPost(id);
		for (Comment comment : comments) {
			cmt.deleteComment(comment.getIdComment().toString());
		}
		Query q=em.createQuery("delete from Post ct where ct.idPost= :id").setParameter("id", Integer.parseInt(id));
		q.executeUpdate();
		return true;
	}

}
