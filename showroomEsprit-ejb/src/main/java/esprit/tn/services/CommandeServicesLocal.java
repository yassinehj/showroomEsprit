package esprit.tn.services;

import java.util.List;

import javax.ejb.Local;

import esprit.tn.entities.Commande;


@Local
public interface CommandeServicesLocal {
	//boolean addCommande(Commande cmd, String idUser);

	Commande findCommandeById(String id);

	boolean updateCommande(String id);

	boolean deleteCommande(String id);

	List<Commande> getCommande();
	public List<Commande> listCommandesToCustomer(String idCustomer);

	boolean addCommande(String idUser, String idArticle);

	List<Commande> getCommandeByArtist(String idArtist);
}
