package esprit.tn.services;



import java.util.List;

import javax.ejb.Local;

import esprit.tn.entities.Comment;

@Local
public interface CommentServicesLocal {


	boolean addComment(Comment cmt, String idUser,String idPost);

	Comment findCommentById(String id);


	boolean updateComment(String id, Comment c);


	boolean deleteComment(String id);

	
	List<Comment> getComments();

	boolean affectCommentToPost(String idComment, String idPost);

	List<Comment> findCommentByPost(String idPost);

	public long calculateCommentsByPost(String idPost);
}
