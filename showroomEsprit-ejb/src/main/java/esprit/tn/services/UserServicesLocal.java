package esprit.tn.services;

import java.util.List;

import javax.ejb.Local;

import esprit.tn.entities.Users;

@Local
public interface UserServicesLocal {

	boolean addUser(Users u);
	List<Users> getUsers();
	Users findById(String id);
	boolean updateUsers(String id,Users user);
	boolean deleteUsers(String id);
	List<Users> findUsersById(List<Integer> id);
	Users authentificationUser(String login,String password);
}
