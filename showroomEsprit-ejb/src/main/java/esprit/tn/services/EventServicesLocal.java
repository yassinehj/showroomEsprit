package esprit.tn.services;

import java.util.List;

import javax.ejb.Local;

import esprit.tn.entities.Event;
import esprit.tn.entities.Users;

@Local
public interface EventServicesLocal {

boolean addEvent(Event e,String idShowroom);
Event findEventById(String id);
boolean affecterUserToEvent(String id,String idUser);
List<Event> findEventByUser(String idUser);
boolean updateEvent(String id,Event event);
List<Event> getAllEvents();
List<Event> getEventsByShowroom(String idShowroom);
public boolean deleteEvent(String id);
long calculateEventByShowroom(String idShowroom);
}
