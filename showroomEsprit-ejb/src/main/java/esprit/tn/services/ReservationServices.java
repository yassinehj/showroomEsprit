package esprit.tn.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
//import java.sql.Date;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import esprit.tn.entities.Article;
import esprit.tn.entities.Post;
import esprit.tn.entities.ReservationShowroom;
import esprit.tn.entities.Showroom;
import esprit.tn.entities.Users;

/**
 * Session Bean implementation class ReservationServices
 */
@Stateless
@LocalBean
public class ReservationServices implements ReservationServicesLocal {
	@PersistenceContext(unitName="showroomEsprit-ejb")
	EntityManager em;
	@EJB
	UserServicesLocal serviceUser;
	@EJB
	ShowroomServicesLocal serviceShowroom;
    /**
     * Default constructor. 
     */
    public ReservationServices() {
        // TODO Auto-generated constructor stub
    }
    @Override
    public boolean reserver(ReservationShowroom reservation,String idArtist,String idShowroom) {
    	Users artist=serviceUser.findById(idArtist);
    	Showroom showroom=serviceShowroom.findShowroomById(idShowroom);
    	reservation.setArtist(artist);
    	reservation.setShowroom(showroom);
    	em.persist(reservation);
    	return true;
    }
    @Override
    public List<ReservationShowroom> listShowroomReseverToArtist(String idArtist) {
		Users u=serviceUser.findById(idArtist);
		Query q=em.createQuery("select r from ReservationShowroom r  where r.artist = :artist ").setParameter("artist", u);
		return q.getResultList();
		
	}
    
    @Override
    public List<ReservationShowroom> listShowroomReseverToCustomer() {
		
		Query q=em.createQuery("select r from ReservationShowroom r ");
		return q.getResultList();
		
	}
    @Override
    public List<ReservationShowroom> listShowroomResever(String idGalleryowner) {
  		Users u=serviceUser.findById(idGalleryowner);
  		Query q=em.createQuery("select r from ReservationShowroom r  where r.showroom.users = :galleryowner ").setParameter("galleryowner", u);
  		return q.getResultList();
  		
  	}
    public String verifyShowroomReserver(String dateInString) {
    	Date now=new Date();
    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    	try {
			Date date = formatter.parse(dateInString);
			Query q=em.createQuery("select count(r.id) from ReservationShowroom r where r.dateReservation=:now")
	    			.setParameter("now", date);
	    	long nbr=(long)q.getSingleResult();
	    	if(nbr>0) {
	    		return "reserver";
	    	}else {
	    		return "ok";
	    	}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "erreur format";
		}
    	//Date date =Date.from(stringDate);
    	//SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");
    	
    }
    
    @Override
    public boolean deleteReservation(String id) {
		Query q=em.createQuery("delete from ReservationShowroom r where r.id= :id").setParameter("id", Integer.parseInt(id));
		q.executeUpdate();
		return true;
    }
    @Override
    public boolean updateReservation(String id,ReservationShowroom rsh) {
    	ReservationShowroom reservation=findReservationById(id);
    	reservation.setDateReservation(rsh.getDateReservation());
    	em.merge(reservation);
    	return true;
    }
    @Override
  	public ReservationShowroom findReservationById(String id) {
  		Query q=em.createQuery("Select a from ReservationShowroom a where a.id= :id").setParameter("id", Integer.parseInt(id));
  		ReservationShowroom art= (ReservationShowroom) q.getSingleResult();
  		//art.getArtists().;
  		return art;
  	}
	@Override
	public long calculateReservationByShowroom(String idShowroom) {
		Showroom sh=serviceShowroom.findShowroomById(idShowroom);
		Query q=em.createQuery("SELECT COUNT(c.id) FROM ReservationShowroom c WHERE c.showroom=:sh").setParameter("sh", sh);
		long cmt= (long)q.getSingleResult();
		return cmt;
	}
	@Override
	public long calculateReservationByShowroomByGallery(String idShowroom,String idUser) {
		Showroom sh=serviceShowroom.findShowroomById(idShowroom);
		Users u=serviceUser.findById(idUser);
		Query q=em.createQuery("SELECT COUNT(c.id) FROM ReservationShowroom c WHERE c.showroom=:sh AND c.showroom.users=:u")
				.setParameter("sh", sh).setParameter("u", u);
		long cmt= (long)q.getSingleResult();
		return cmt;
	}
	@Override
	public List<ReservationShowroom> listReservationShowroomofGallery(String idUser){
		Users u=serviceUser.findById(idUser);
		Query q=em.createQuery("SELECT c FROM ReservationShowroom c WHERE c.showroom.users=:u")
				.setParameter("u", u);
		return q.getResultList();
		
	}
	
}
