package esprit.tn.services;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;
import javax.persistence.Query;

import esprit.tn.entities.Article;
import esprit.tn.entities.ReservationShowroom;

@Local
public interface ReservationServicesLocal {
	boolean reserver(ReservationShowroom reservation,String idArtist,String idShowroom);
	List<ReservationShowroom> listShowroomReseverToArtist(String idArtist);
	List<ReservationShowroom> listShowroomResever(String idGalleryowner);
	 boolean deleteReservation(String id) ;
	 boolean updateReservation(String id,ReservationShowroom rsh);
	ReservationShowroom findReservationById(String id);
	String verifyShowroomReserver(String dateInString);
	public List<ReservationShowroom> listShowroomReseverToCustomer();
	long calculateReservationByShowroom(String idShowroom);
	long calculateReservationByShowroomByGallery(String idShowroom, String idUser);
	List<ReservationShowroom> listReservationShowroomofGallery(String idUser);
}