package esprit.tn.services;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import esprit.tn.entities.Comment;
import esprit.tn.entities.Post;
import esprit.tn.entities.Users;

/**
 * Session Bean implementation class CommentServices
 */
@Stateless
@LocalBean
public class CommentServices implements CommentServicesLocal {

	@PersistenceContext(unitName="showroomEsprit-ejb")
	EntityManager em;
	@EJB
	UserServicesLocal serviceUser;
	@EJB
	PostServicesLocal servicePost;

    /**
     * Default constructor. 
     */
    public CommentServices() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public boolean addComment(Comment cmt, String idUser,String idPost) {
		
		
		Users users=serviceUser.findById(idUser);
		Post post=servicePost.findPostById(idPost);
		cmt.setUsers(users);
		cmt.setPost(post);
			em.persist(cmt);
			return true;

	}

	@Override
	public Comment findCommentById(String id) {
		Query q=em.createQuery("Select c from Comment c where c.idComment= :id").setParameter("id", Integer.parseInt(id));
		Comment cmt= (Comment) q.getSingleResult();
		return cmt;

	}
	@Override
	public List<Comment> findCommentByPost(String idPost) {
		Post post=servicePost.findPostById(idPost);
		Query q=em.createQuery("Select c from Comment c where c.post= :Post").setParameter("Post", post);
		List<Comment> cmt= q.getResultList();
		return cmt;

	}

	@Override
	public boolean updateComment(String id,Comment cmt) {
		Comment com=findCommentById(id);
    	com.setDescription(cmt.getDescription());
    	
    	
    	em.merge(com);
    	return true;

	}

	@Override
	public boolean deleteComment(String id) {
		Query q=em.createQuery("delete from Comment c where c.idComment= :id").setParameter("id", Integer.parseInt(id));
		q.executeUpdate();
		return true;

	}

	@Override
	public List<Comment> getComments() {
		Query q=em.createQuery("select c from Comment c");
		return q.getResultList();
	}

	@Override
	public boolean affectCommentToPost(String idComment, String idPost) {
		Comment cmt=findCommentById(idComment);
		Post p=servicePost.findPostById(idPost);
		cmt.setPost(p);
		
		em.merge(cmt);
		return true;


	}
	@Override
	public long calculateCommentsByPost(String idPost) {
		Post post=servicePost.findPostById(idPost);
		Query q=em.createQuery("SELECT COUNT(c.idComment) FROM Comment c WHERE c.post=:Post").setParameter("Post", post);
		long cmt= (long)q.getSingleResult();
		return cmt;

	}
	

}
