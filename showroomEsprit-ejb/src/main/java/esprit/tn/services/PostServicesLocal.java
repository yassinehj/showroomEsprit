package esprit.tn.services;

import java.util.List;

import javax.ejb.Local;

import esprit.tn.entities.Post;

@Local
public interface PostServicesLocal {
	boolean addPost(Post ct, String idUser);
	List<Post> getPost();
	Post findPostById(String id);
	boolean updatePost(String id,Post p);
	boolean deletePost(String id);
	public List<Post> listPostToUser(String idUser) ;
}
