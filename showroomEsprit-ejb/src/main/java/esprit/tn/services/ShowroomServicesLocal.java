package esprit.tn.services;

import java.util.List;

import javax.ejb.Local;

import esprit.tn.entities.Showroom;
import esprit.tn.entities.Users;

@Local
public interface ShowroomServicesLocal {
	
	boolean addShowroom(Showroom sh, String idUser);
	List<Showroom> getShowroom();
	Showroom findShowroomById(String id);
	boolean updateShowroom(String id,Showroom sh);
	boolean deleteShowroom(String id);
	//boolean affecterUserToShowroom(String id,String idUser);
	List<Showroom> getShowroomByUser(String idUser);
}
