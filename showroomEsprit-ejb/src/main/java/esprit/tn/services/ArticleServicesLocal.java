package esprit.tn.services;

import java.util.List;

import javax.ejb.Local;

import esprit.tn.entities.Article;

@Local
public interface ArticleServicesLocal {

	boolean addArticle(Article art, String idUser,String idShowroom);

	Article findArticleById(String id);

	boolean updateArticle(String id,Article art);

	boolean deleteArticle(String id);

	List<Article> getArticles();

	//boolean affecterArticleToCommande(String idArticle, String idCommande);

	List<Article> getArticlesByUser(String idUser);

	List<Article> getArticlesByShowroom(String idShowroom);

	List<Article> getArticlesByShowroomByArtist(String idShowroom, String idArtist);

	boolean articleCommander(String id);

	boolean articleNonCommander(String id);

}
