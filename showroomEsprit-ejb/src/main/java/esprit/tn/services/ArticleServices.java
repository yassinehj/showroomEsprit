package esprit.tn.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.registry.infomodel.User;

import esprit.tn.entities.Article;
import esprit.tn.entities.Commande;
import esprit.tn.entities.Event;
import esprit.tn.entities.Showroom;
import esprit.tn.entities.Users;

/**
 * Session Bean implementation class ArticleServices
 */
@Stateless
public class ArticleServices implements ArticleServicesLocal {
	@PersistenceContext(unitName="showroomEsprit-ejb")
	EntityManager em;
	@EJB
	UserServicesLocal serviceUser;
	@EJB
	CommandeServicesLocal serviceCommande;
	@EJB
	ShowroomServicesLocal serviceShowroom;
    /**
     * Default constructor. 
     */
    public ArticleServices() {
        // TODO Auto-generated constructor stub
    }
    
    public boolean addArticle(Article art,String idUser,String idShowroom) {
    	Users artists=serviceUser.findById(idUser);
    	Showroom showroom=serviceShowroom.findShowroomById(idShowroom);
		art.setArtists(artists);
		art.setShowroom(showroom);
			em.persist(art);
			return true;
		
	}
    @Override
	public List<Article> getArticles() {
		Query q=em.createQuery("select a from Article a");
		return q.getResultList();
	}
    @Override
	public List<Article> getArticlesByUser(String idUser ) {
    	Users user=serviceUser.findById(idUser);
		Query q=em.createQuery("select a from Article a where a.artists=:user").setParameter("user", user);
		return q.getResultList();
	}
    @Override
   	public List<Article> getArticlesByShowroom(String idShowroom ) {
       	Showroom showroom=serviceShowroom.findShowroomById(idShowroom);
   		Query q=em.createQuery("select a from Article a where a.showroom=:showroom").setParameter("showroom", showroom);
   		return q.getResultList();
   	}
    @Override
   	public List<Article> getArticlesByShowroomByArtist(String idShowroom,String idArtist ) {
    	Users u=serviceUser.findById(idArtist);
       	Showroom showroom=serviceShowroom.findShowroomById(idShowroom);
   		Query q=em.createQuery("select a from Article a where a.showroom=:showroom and a.artists=:user")
   				.setParameter("showroom", showroom).setParameter("user", u);
   		return q.getResultList();
   	}
    @Override
	public Article findArticleById(String id) {
		Query q=em.createQuery("Select a from Article a where a.idArticle= :id").setParameter("id", Integer.parseInt(id));
		Article art= (Article) q.getSingleResult();
		//art.getArtists().;
		return art;
	}
    @Override
    public boolean updateArticle(String id,Article art) {
    	Article article=findArticleById(id);
    	article.setArticleDescription(art.getArticleDescription());
    	article.setArticleName(art.getArticleName());
    	article.setPrix(art.getPrix());
    	article.setArtists(article.getArtists());
    	article.setImageUrl(art.getImageUrl());
    	em.merge(article);
    	return true;
    }
    public boolean deleteArticle(String id) {
		Query q=em.createQuery("delete from Article a where a.idArticle= :id").setParameter("id", Integer.parseInt(id));
		q.executeUpdate();
		return true;

	}
    @Override
    public boolean articleCommander(String id) {
    	Article article=findArticleById(id);
    	article.setCommander(1);

    	em.merge(article);
    	return true;
    }
    @Override
    public boolean articleNonCommander(String id) {
    	Article article=findArticleById(id);
    	article.setCommander(0);

    	em.merge(article);
    	return true;
    }
    
	/*public boolean affecterArticleToCommande(String idArticle, String idCommande) {
		Article art=findArticleById(idArticle);
		Commande c=serviceCommande.findCommandeById(idCommande);
		art.setCommande(c);
		double prixTot=c.getPrixTotal()+art.getPrix();
		
		c.setPrixTotal(prixTot);
		//u.getEvents().add(e);
		//if(e.getCustomers() == null)
			//e.setCustomers(new ArrayList<Users>());
		//e.getCustomers().add(u);
//		List<Users> l=e.getCustomers();
//		if(l == null) {
//			l=new ArrayList<Users>();
//		}
//		l.add(u);
//		e.setCustomers(l);
		//em.merge(e);
		em.merge(art);
		em.merge(c);
		return true;
	}*/

}
