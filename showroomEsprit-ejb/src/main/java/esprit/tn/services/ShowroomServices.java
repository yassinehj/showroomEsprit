package esprit.tn.services;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import esprit.tn.entities.Article;
import esprit.tn.entities.Showroom;
import esprit.tn.entities.Users;

/**
 * Session Bean implementation class ShowroomServices
 */
@Stateless
@LocalBean
public class ShowroomServices implements ShowroomServicesLocal {

	@PersistenceContext(unitName="showroomEsprit-ejb")
	EntityManager em;
	@EJB
	UserServicesLocal usl;
    /**
     * Default constructor. 
     */
    public ShowroomServices() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public boolean addShowroom(Showroom sh, String idUser) {
		Users users = usl.findById(idUser);
		sh.setUsers(users);
		em.persist(sh);
		return true;
	}

	@Override
	public List<Showroom> getShowroom() {
		Query q=em.createQuery("select sh from Showroom sh");
		return q.getResultList();
		
	}

	@Override
	public Showroom findShowroomById(String id) {
		Query q=em.createQuery("Select sh from Showroom sh where sh.idShowroom= :id").setParameter("id", Integer.parseInt(id));
		return (Showroom) q.getSingleResult();
	}

	@Override
	public boolean updateShowroom(String id,Showroom sh) {
		
        Showroom showroom=findShowroomById(id);
    	
    	showroom.setAdress(sh.getAdress());
    	showroom.setDescription(sh.getDescription());
    	showroom.setEquipmentIncluded(sh.getEquipmentIncluded());
    	showroom.setNomShowroom(sh.getNomShowroom());
    	showroom.setSurface(sh.getSurface());
    	showroom.setImageShowroom(sh.getImageShowroom());
		
		em.merge(showroom);
		return true;
	}

	@Override
	public boolean deleteShowroom(String id) {
		Query q=em.createQuery("delete from Showroom sh where sh.idShowroom= :id").setParameter("id", Integer.parseInt(id));
		q.executeUpdate();
		return true;
	}
	@Override
	public List<Showroom> getShowroomByUser(String idUser ) {
    	Users user=usl.findById(idUser);
		Query q=em.createQuery("select a from Showroom a where a.users=:user").setParameter("user", user);
		return q.getResultList();
	}
	

//	@Override
//	public boolean affecterUserToShowroom(String id, String idUser) {
//		// TODO Auto-generated method stub
//		return false;
//	}

	
	

}
