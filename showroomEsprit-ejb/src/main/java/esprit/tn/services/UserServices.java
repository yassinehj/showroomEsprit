package esprit.tn.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import esprit.tn.entities.Users;

/**
 * Session Bean implementation class UserServices
 */
@Stateless
public class UserServices implements UserServicesLocal {

	@PersistenceContext(unitName="showroomEsprit-ejb")
	EntityManager em;
    /**
     * Default constructor. 
     */
    public UserServices() {
        // TODO Auto-generated constructor stub
    }
	@Override
	public boolean addUser(Users u) {
		if(u.getRole().equals("")) {
			return false;
		}else {
			em.persist(u);
			return true;
		}
		
		
	}
	@Override
	public List<Users> getUsers() {
		Query q=em.createQuery("select u from Users u");
		return q.getResultList();
	}
	@Override
	public Users findById(String id) {
		Query q=em.createQuery("Select u from Users u where u.id= :id").setParameter("id", Integer.parseInt(id));
		return (Users) q.getSingleResult();
	}
	@Override
	public List<Users> findUsersById(List<Integer> id) {
		Query q=em.createQuery("Select u from Users u where u.id in :id").setParameter("id", id);
		return  q.getResultList();
	}
	
	public boolean updateUsers(String id,Users user) {
		//Users u=findById(id);
		
		em.merge(user);
		return true;
	}
	public boolean deleteUsers(String id) {
		Query q=em.createQuery("delete from Users u where u.id= :id").setParameter("id", Integer.parseInt(id));
		q.executeUpdate();
		return true;

	}
	@Override
	public Users authentificationUser(String login, String password) {
		try {
			Query q=em.createQuery("Select u from Users u where u.login= :login and u.password= :password").setParameter("login", login)
					.setParameter("password", password);
			return (Users) q.getSingleResult();
		} catch (Exception e) {
		e.printStackTrace();
		return null;
		}
	}
	
	

}
